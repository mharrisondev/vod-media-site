import React from 'react';
import './scss/App.scss';
import NavigationPrimary from './components/NavigationPrimary';
import CarouselContainer from './components/CarouselContainer';
import Loading from './components/Loading';
import VideoModal from './components/VideoModal';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      displayModal: false,
      modalData: null,
    };
  }

  componentDidMount() {
    /* Fetch Data with arbitrary wait time */
    fetch('https://demo2697834.mockable.io/movies')
      .then(response => response.json())
      .then(data => this.dataCaching(data));
  }

  dataCaching = (data) => {

    /*
      Exersize had an optional requirement to cache images,
      but the source (lorempixel.com) used http headers 'no-cache' && 'no-store'.
      Below I am adding a new key to each Movie object,
      with a new random image source that supports caching.
    */

    /* Wrap fetch requests in a promise that will only resolve when all fetches complete */
    var promises = data.entries.map((obj, index) => {
      return fetch('https://source.unsplash.com/random/230x320?' + index) // ?index to get unique images.
      .then(response => {
        obj.cachedImage = response.url;
        return obj;
      })
    });

    Promise.all(promises)
      .then( data => this.setState({ loading: false, data: data }) );
  }

  showModal = (data) => {
    console.log('showModal()');
    this.setState({
      displayModal: true,
      modalData: data,
     });
    document.body.classList.add('modal-open');
  };

  hideModal = () => {
    console.log('hideModal()');
    this.setState({
      displayModal: false
    });
    document.body.classList.remove('modal-open');
  };

  updateData = (modalData) => {
    console.log('updateData()');

    /*
    Loop existing state data, find which video is playing in modal,
    and add/update lastWatched key
    */
    if (modalData.id) {
      this.state.data.forEach((obj, index) => {
        for (var key in obj) {
          if (obj.hasOwnProperty(key)) {
            if (obj['id'] === modalData.id) {
              // generate unix timestamp
              return obj.lastWatched = new Date().getTime();
            }
          }
        }
      });
    }
  }

  render() {
    function LoadingTemplate(props) {
      return (
        <div className="App">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <Loading />
              </div>
            </div>
          </div>
        </div>
      );
    }

    function AppTemplate(props) {
      return (
        <div className="App">
          <div className="container primary-container">
            <div className="row">
              <div className="col-12">
                <NavigationPrimary />
                <CarouselContainer
                  type="Unwatched"
                  videoData={props.state.data}
                  showModal={props.showModal} />
                <CarouselContainer
                  type="Watched"
                  videoData={props.state.data}
                  showModal={props.showModal} />
              </div>
            </div>
          </div>
          <VideoModal state={props.state} hideModal={props.hideModal} updateData={props.updateData} />
        </div>
      );
    }

    if (this.state.loading) {
      return (
        <LoadingTemplate />
      );
    } else {
      return (
        <AppTemplate
          state={this.state}
          hideModal={this.hideModal}
          showModal={this.showModal}
          updateData={this.updateData} />
      );
    }
  }
}

export default App;
