import React from 'react';
import VideoCarousel from './VideoCarousel';

class CarouselContainer extends React.Component {

  loopVideoData = (videoData, carouselType, unwatched, watched) => {
    videoData.forEach( (obj, index) => {
      if (obj.lastWatched) {
        watched.push(obj);
      } else {
        unwatched.push(obj);
      }
    });
  }

  /* Compare UTC time strings */
  sortNewestUtcTime = (a, b) => {
    if (a.lastWatched > b.lastWatched) {
      return -1;
    }
    if (a.lastWatched < b.lastWatched) {
      return 1;
    }
    return 0;
  }

  render() {

    /*
      Check which carousel type and add videos accordingly
    */

    let videoData = this.props.videoData;
    let carouselType = this.props.type;
    let unwatched = [];
    let watched = [];
    let carouselVideos;

    if (this.props.type) {
      this.loopVideoData(videoData, carouselType, unwatched, watched)
    }

    /* Sort Watched videos by most recently watched first */
    if (watched.length > 1) {
      watched.sort(this.sortNewestUtcTime);
    }

    carouselType === 'Watched' ? carouselVideos = watched : carouselVideos = unwatched;

    if (carouselVideos.length > 0) {
      return(
        <div className="carousel-container">
          <div className="carousel-container__title">
            <h1>{this.props.type}</h1>
          </div>
          <div className="carousel-container__carousel">
            <VideoCarousel
              videoData={carouselVideos}
              showModal={this.props.showModal} />
          </div>
        </div>
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}

export default CarouselContainer;
