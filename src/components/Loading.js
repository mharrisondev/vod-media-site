import React from 'react';

function Loading() {
  return (
    <div className="loading-container text-center d-flex flex-column justify-content-center align-items-center">
      <h1>
        <i className="fas fa-spinner fa-pulse"></i>
      </h1>
      <h1>Loading data</h1>
    </div>
  );
}

export default Loading;
