import React from 'react';

class VideoThumb extends React.PureComponent {


  handleClick = () => {
    this.props.showModal(this.props.videoData);
  }

  render() {
    let image = this.props.videoData.cachedImage ? this.props.videoData.cachedImage : 'https://via.placeholder.com/250x350'
    let inlineStyle = {
      backgroundImage: 'url(' + image + ')',
    }

    return (
      <div className="video-thumb" onClick={() => this.props.showModal(this.props.videoData)} >
        <div className="video-thumb__image" style={inlineStyle}>
          <div className="video-thumb__play-icon">
            <i className="fas fa-play fa-2x"></i>
          </div>
          <div className="video-thumb__title">
            <span>{this.props.videoData.title}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default VideoThumb;
