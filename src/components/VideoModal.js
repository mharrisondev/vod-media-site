import React from 'react';

class VideoModal extends React.Component {

  componentDidMount() {
    // console.log('MODAL componentDidMount()');
    if (this.props.state.modalData) {
      this.props.updateData(this.props.state.modalData);
    }
  }

  componentWillUnmount() {
    // console.log('MODAL componentWillUnmount()');
    if (this.props.state.modalData) {
      this.props.updateData(this.props.state.modalData);
    }
  }

  render() {
    let displayState = this.props.state.displayModal ? 'show' : 'hide';
    let modalData = this.props.state.modalData;

    if (modalData) {
      let lastWatched = modalData.lastWatched ? <div><strong>Last viewed:</strong> {new Date(modalData.lastWatched).toDateString()}</div> : null;
      let mediaType = modalData.type ? <div><strong>Media Type:</strong> {modalData.type}</div> : null;
      let description = modalData.description ? <div>{modalData.description}</div> : null;
      let availableDate = modalData.availableDate ? <div><strong>Available Date:</strong> {new Date(modalData.availableDate).toDateString()}</div> : null;
      let publishedDate = modalData.publishedDate ? <div><strong>Published Date:</strong> {new Date(modalData.publishedDate).toDateString()}</div> : null;
      let parentalRatings = modalData.parentalRatings[0] ? <div><strong>Parental Rating:</strong> { modalData.parentalRatings[0].rating } - { modalData.parentalRatings[0].scheme }</div> : null;
      let categories = "";

      modalData.categories.forEach((ele, index) => {
        if (ele.title) {
          categories = categories + ele.title + " ";
        }
      });

      return (
        <div id="videoModal" className={displayState} onClick={ this.handleClick }>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="video-container">
                  <button className="close-btn btn" onClick={ this.props.hideModal }>
                    <i className="fas fa-times fa-2x"></i>
                  </button>
                  <video controls preload="auto">
                    <source src={ modalData.contents[0].url } type="video/mp4" />
                    Sorry, your browser doesnt support embedded videos.
                  </video>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="video-info">
                  <h2>{modalData.title}</h2>
                  <strong>{ categories }</strong>
                  { description }
                </div>
              </div>
              <div className="col-12">
                <div className="video-details mt-3 mb-3">
                  { lastWatched }
                  { mediaType }
                  { parentalRatings }
                  { availableDate }
                  { publishedDate }
                </div>
              </div>
              <div className="col-12 last-col">
                <button className="close-btn-description btn btn-sm btn-danger" onClick={ this.props.hideModal }>
                  Close window
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}

export default VideoModal;
