import React from 'react';
import Slider from "react-slick";
import VideoThumb from './VideoThumb';

class VideoCarousel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      viewportWidth: null
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  updateDimensions = () => {
    this.setState({viewportWidth: document.documentElement.clientWidth});
  }

  componentWillMount = () => {
      this.updateDimensions();
  }

  componentDidMount = () => {
      window.addEventListener("resize", this.updateDimensions);
  }

  componentWillUnmount = () => {
      window.removeEventListener("resize", this.updateDimensions);
  }

  render() {
    let videoData = this.props.videoData;
    let videoThumbArray = [];

    for (var key in videoData) {
      if (videoData.hasOwnProperty(key)) {
        videoThumbArray.push(
          <VideoThumb
            key={videoData[key].id}
            videoData={videoData[key]}
            showModal={this.props.showModal} />
        )
      }
    }

    let settings = {
      dots: true,
      arrows: true,
      infinite: false,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 5,
      rows: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 767,
          settings: "unslick"
        }
      ]
    };

    if (this.state.viewportWidth > 767) {
      return (
        <Slider {...settings}>
          {videoThumbArray}
        </Slider>
      );
    } else {
      return (
        <div>{videoThumbArray}</div>
      );
    }
  }
}

export default VideoCarousel;
