import React from 'react';

class NavigationPrimary extends React.Component {
  render() {
    return (
      <div className="p-navigation">
        <div className="p-navigation__title">
          <h1><i className="fas fa-film"></i> Video on Demand</h1>
        </div>
      </div>
    );
  }
}

export default NavigationPrimary;
